use x86_64::structures::idt::*;

extern "x86-interrupt" fn breakpoint_fn(stack_frame: &mut ExceptionStackFrame) {
    println!("BREAKPOINT");
}

extern "x86-interrupt" fn divide_by_zero(_: &mut ExceptionStackFrame) {
    println!("DIVIDE BY ZERO");
}
lazy_static! {
    static ref IDT: Idt = {
        let mut idt = Idt::new();
        idt.breakpoint.set_handler_fn(breakpoint_fn);
        idt.divide_by_zero.set_handler_fn(divide_by_zero);
        idt
    };
}
pub fn init() {
    IDT.load();
}
