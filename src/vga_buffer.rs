use core::ptr::Unique;
use volatile::Volatile;
use spin::Mutex;
use core::fmt::Write;
pub static STDOUT: Mutex<Writer> = Mutex::new(Writer {
    column_position: 0,
    colour_code: ColourCode::new(Colour::White, Colour::Blue),
    buffer: unsafe { Unique::new_unchecked(0xb8000 as *mut _) },
});

macro_rules! println {
    ($fmt:expr) => (print!(concat!($fmt, "\n")));
    ($fmt:expr, $($arg:tt)*) => (print!(concat!($fmt, "\n"),$($arg)*));
}
macro_rules! print {
    ($($arg:tt)*) => ( {
        $crate::vga_buffer::print(
        format_args!($($arg)*))});
}

pub fn print(args: fmt::Arguments) {
    write!(STDOUT.lock(), "{}", args).unwrap();
}

#[allow(dead_code)]
#[repr(u8)]
#[derive(Clone, Copy)]
pub enum Colour {
    Black = 0,
    Blue = 1,
    Green = 2,
    Red = 4,
    White = 15,
}
#[derive(Clone, Copy)]
struct ColourCode(u8);
impl ColourCode {
    const fn new(fg: Colour, bg: Colour) -> ColourCode {
        ColourCode((bg as u8) << 4 | (fg as u8))
    }
}

#[repr(C)]
#[derive(Clone, Copy)]
struct ScreenChar {
    ascii_character: u8,
    colour_code: ColourCode,
}
const ROWS: usize = 25;
const COLUMNS: usize = 80;

struct Buffer {
    chars: [[Volatile<ScreenChar>; COLUMNS]; ROWS],
}

pub struct Writer {
    column_position: usize,
    colour_code: ColourCode,
    buffer: Unique<Buffer>,
}

impl Writer {
    pub fn write_byte(&mut self, byte: u8) {
        match byte {
            b'\n' => self.new_line(),
            byte => {
                if self.column_position >= COLUMNS {
                    self.new_line()
                }
                let col = self.column_position;
                let row = ROWS - 1;
                let colour_code = self.colour_code;
                self.buffer().chars[row][col].write(ScreenChar {
                    ascii_character: byte,
                    colour_code: colour_code,
                });
                self.column_position += 1;
            }
        }
    }

    fn buffer(&mut self) -> &mut Buffer {
        unsafe { self.buffer.as_mut() }
    }

    fn new_line(&mut self) {
        for row in 1..ROWS {
            for col in 0..COLUMNS {
                let buffer = self.buffer();
                let character = buffer.chars[row][col].read();
                buffer.chars[row - 1][col].write(character);
            }
        }
        self.clear_row(ROWS - 1);
        self.column_position = 0;
    }
    fn clear_row(&mut self, row: usize) {
        let blank = ScreenChar {
            ascii_character: b' ',
            colour_code: ColourCode(0),
        };
        for i in 0..COLUMNS {
            self.buffer().chars[row][i].write(blank)
        }
    }
}
use core::fmt;
impl fmt::Write for Writer {
    fn write_str(&mut self, s: &str) -> fmt::Result {
        for byte in s.bytes() {
            self.write_byte(byte)
        }
        Ok(())
    }
}

impl Default for Writer {
    fn default() -> Writer {
        Writer {
            column_position: 0,
            colour_code: ColourCode::new(Colour::White, Colour::Blue),
            buffer: unsafe { Unique::new_unchecked(0xb8000 as *mut _) },
        }
    }
}
