#![feature(lang_items)]
#![feature(const_fn)]
#![feature(unique)]
#![feature(ptr_internals)]
#![no_std]
#![feature(abi_x86_interrupt)]
#[macro_use]
extern crate lazy_static;
extern crate rlibc;
extern crate spin;
extern crate volatile;
extern crate x86_64;
#[macro_use]
mod vga_buffer;
mod exceptions;

#[no_mangle]
pub extern "C" fn rust_main() {
    exceptions::init();
    println!("Boot successful");
}
#[lang = "eh_personality"]
#[no_mangle]
pub extern "C" fn eh_personality() {}
#[lang = "panic_fmt"]
#[no_mangle]
pub extern "C" fn panic_fmt() -> ! {
    loop {}
}
